// Instruction 3-4

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json()) 
.then((json)=> {let titleArray = json.map(function(title){
	return title.title;
})
// or .then((json)=> {let titleArray = json.map((title)=>title.title)
	
console.log(titleArray);

});



//Instruction 5-6

fetch ('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json()) 
.then((json)=> console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// Instruction 7

fetch ('https://jsonplaceholder.typicode.com/todos/', {

	method: 'POST',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1,
		id: 201
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


// Instruction 8-9

fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		descriptioin: 'To update my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	})
})

.then((response)=>response.json())
.then((json)=>console.log(json));

// Instruction 10-11

fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PATCH',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
		dateCompleted: "07/09/21"
	})
})

.then((response)=>response.json())
.then((json)=>console.log(json));

// Instruction 12
fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'DELETE',
})

.then((response)=>response.json())
.then((json)=>console.log(json));
